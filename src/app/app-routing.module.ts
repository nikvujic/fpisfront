import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProstSlucajComponent } from './prost-slucaj/prost-slucaj.component';
import { SlozenSlucajComponent } from './slozen-slucaj/slozen-slucaj.component';
import { WelcomePageCompoenentComponent } from './welcome-page-compoenent/welcome-page-compoenent.component';


const routes: Routes = [
  {path: '', component: WelcomePageCompoenentComponent},
  {path: 'prostSlucaj', component: ProstSlucajComponent},
  {path: 'slozenSlucaj', component: SlozenSlucajComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
