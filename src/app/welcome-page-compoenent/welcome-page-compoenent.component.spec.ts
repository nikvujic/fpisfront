import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomePageCompoenentComponent } from './welcome-page-compoenent.component';

describe('WelcomePageCompoenentComponent', () => {
  let component: WelcomePageCompoenentComponent;
  let fixture: ComponentFixture<WelcomePageCompoenentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomePageCompoenentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomePageCompoenentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
