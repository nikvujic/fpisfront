import { Absence } from "./absence.model";
import { Attendance } from "./attendance.model";

export class RadnaListaDto {
  constructor(
    public datum: string,
    public radniNalogId: number,
    public radnik: {id: number},
    public tovarniListId: number,
    public odsustva: Absence[],
    public prisustva: Attendance[]
  ) {}
}

