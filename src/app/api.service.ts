import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Absence } from './absence.model';
import { Attendance } from './attendance.model';
import { ProductionOrder } from './production-order.model';
import { Proizvod } from './proizvod.model';
import { RadnaListaDto } from './radna-lista-dto.model';
import { RadnaLista } from './radna-lista.model';
import { TipProizvoda } from './tip-proizvoda.model';
import { Waybill } from './waybill.model';
import { Worker } from './worker.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  async getTipoviProizvoda() {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/products/types`, {headers: header}).toPromise();

      let tipoviProizvoda: TipProizvoda[] = [];

      response.forEach(resTip => {
        const newTipProizvoda = new TipProizvoda(
          resTip.id,
          resTip.boja,
          resTip.naziv,
          resTip.normativId,
        )

        tipoviProizvoda.push(newTipProizvoda);
      });

      return tipoviProizvoda;
    } catch (error) {
      console.log("getTipoviProizvoda error:", error);

      throw error;
    }
  }

  async saveProizvod(debljina: string, duzina: string, sirina: string, visina: string, tipProizvodaId: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let requestObject = {
        "debljina": debljina,
        "duzina": duzina,
        "sirina": sirina,
        "visina": visina,
        "tipProizvodaId": tipProizvodaId
    }

    try {
      let response: any = await this.http.post(`${environment.server}/api/v1/products/`, {...requestObject}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("saveProizvod error:", error);

      throw error;
    }
  }

  async getProizvod(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/products/${id}`, {headers: header}).toPromise();
      let tipoviProizvoda: TipProizvoda[] = await this.getTipoviProizvoda();

      let newTipProizvoda;

      tipoviProizvoda.forEach(tipProizvoda => {
        if (tipProizvoda.id == response.tipProizvodaId) {
          newTipProizvoda = tipProizvoda;
        }
      });

      const newProizvod = new Proizvod(
        response.id,
        response.debljina,
        response.duzina,
        response.sirina,
        response.visina,
        newTipProizvoda
      )

      return newProizvod;
    } catch (error) {
      console.log("getProizvod error:", error);

      throw error;
    }
  }

  async getProizvodi() {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/products`, {headers: header}).toPromise();
      let tipoviProizvoda: TipProizvoda[] = await this.getTipoviProizvoda();

      let proizvodi: Proizvod[] = [];

      response.forEach(proizvod => {
        let newTipProizvoda;

        tipoviProizvoda.forEach(tipProizvoda => {
          if (tipProizvoda.id == proizvod.tipProizvodaId) {
            newTipProizvoda = tipProizvoda;
          }
        });

        const newProizvod = new Proizvod(
          proizvod.id,
          proizvod.debljina,
          proizvod.duzina,
          proizvod.sirina,
          proizvod.visina,
          newTipProizvoda
        )

        proizvodi.push(newProizvod);
      });

      return proizvodi;
    } catch (error) {
      console.log("getProizvodi error:", error);

      throw error;
    }
  }

  async editProizvod(id: number, debljina: string, duzina: string, sirina: string, visina: string, tipProizvodaId: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let requestObject = {
      "debljina": debljina,
      "duzina": duzina,
      "sirina": sirina,
      "visina": visina,
      "tipProizvodaId": tipProizvodaId
    }

    try {
      let response: any = await this.http.put(`${environment.server}/api/v1/products/${id}`, {...requestObject}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("editProizvod error:", error);

      throw error;
    }
  }

  async deleteProizvod(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.delete(`${environment.server}/api/v1/products/${id}`, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("deleteProizvod error:", error);

      throw error;
    }
  }

  async getRadnik(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/workers/${id}`, {headers: header}).toPromise();

      let radnik: Worker = new Worker(
        response.id,
        response.jmbg,
        response.imePrezime,
        response.radnoMesto
      );

      return radnik;
    } catch (error) {
      console.log("getRadnik error:", error);

      throw error;
    }
  }

  async getTovarnaLista(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/things/waybill/${id}`, {headers: header}).toPromise();

      let tovarnaLista: Waybill = new Waybill(
        response.id,
        response.vrstaPosiljke,
        response.nacinPrevoza,
        response.radnikId,
        response.klijentId
      );

      return tovarnaLista;
    } catch (error) {
      console.log("getTovarnaLista error:", error);

      throw error;
    }
  }

  async getRadniNalog(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/things/production-order/${id}`, {headers: header}).toPromise();

      let radniNalog: ProductionOrder = new ProductionOrder(
        response.id,
        response.datum,
        response.brojPonude
      );

      return radniNalog;
    } catch (error) {
      console.log("getRadniNalog error:", error);

      throw error;
    }
  }

  async saveRadnaLista(radnaLista: RadnaListaDto) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.post(`${environment.server}/api/v1/worksheets`, {...radnaLista}, {headers: header}).toPromise();
      return response;
    } catch (error) {
      console.log("saveRadnaLista error:", error);

      throw error;
    }
  }

  async getRadnaLista(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/api/v1/worksheets/${id}`, {headers: header}).toPromise();

      const radnik = new Worker(
        response.radnik.id,
        response.radnik.jmbg,
        response.radnik.imePrezime,
        response.radnik.radnoMesto
      );

      const odsustva: Absence[] = [];
      const prisustva: Attendance[] = [];

      response.odsustva.forEach(odsustvo => {
        odsustva.push(odsustvo);
      });

      response.prisustva.forEach(prisustvo => {
        prisustva.push(prisustvo);
      });

      let radnaLista: RadnaLista = new RadnaLista(
        response.id,
        response.datum,
        response.radniNalogId,
        response.tovarniListId,
        radnik,
        odsustva,
        prisustva
      );

      return radnaLista;
    } catch (error) {
      console.log("getRadnaLista error:", error);

      throw error;
    }
  }

  async editRadnaLista(radnaLista: RadnaLista) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.put(`${environment.server}/api/v1/worksheets/${radnaLista.id}`, {...radnaLista}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("editRadnaLista error:", error);

      throw error;
    }
  }

  async deleteRadnaLista(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.delete(`${environment.server}/api/v1/worksheets/${id}`, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("deleteRadnaLista error:", error);

      throw error;
    }
  }
}
