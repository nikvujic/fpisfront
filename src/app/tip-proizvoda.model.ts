export class TipProizvoda {
  constructor(
    public id: number,
    public boja: string,
    public naziv: string,
    public normativId: number
  ) {}
}

