import { TipProizvoda } from "./tip-proizvoda.model";

export class Proizvod {
  constructor(
    public id: number,
    public debljina: string,
    public duzina: string,
    public sirina: string,
    public visina: string,
    public tipProizvoda: TipProizvoda
  ) {}
}

