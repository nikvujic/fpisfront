import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProstSlucajComponent } from './prost-slucaj.component';

describe('ProstSlucajComponent', () => {
  let component: ProstSlucajComponent;
  let fixture: ComponentFixture<ProstSlucajComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProstSlucajComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProstSlucajComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
