import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { ApiService } from 'src/app/api.service';
import { TipProizvoda } from 'src/app/tip-proizvoda.model';

@Component({
  selector: 'app-novi-proizvod',
  templateUrl: './novi-proizvod.component.html',
  styleUrls: ['./novi-proizvod.component.scss']
})
export class NoviProizvodComponent implements OnInit {

  debljina: string = "";
  debljinaErrorMessage: string = "";

  duzina: string = "";
  duzinaErrorMessage: string = "";

  sirina: string = "";
  sirinaErrorMessage: string = "";

  visina: string = "";
  visinaErrorMessage: string = "";

  tipoviProizvoda: TipProizvoda[]= [];

  selektovaniTipProzivoda: TipProizvoda;

  constructor(
    private dialogRef: MatDialogRef<NoviProizvodComponent>,
    private apiService: ApiService,
    private snackBar: MatSnackBar
  ) { }

  openSnackBar(message: string, action = "") {
    this.snackBar.open(message, action, {duration: 5000});
  }

  ngOnInit() {
    this.ucitajTipoveProizvoda();
  }

  onTipProzivodaSelect(tipProzivoda) {
    this.selektovaniTipProzivoda = tipProzivoda;
  }

  onDodaj() {
    if (this.validateFields()) {
      if (this.selektovaniTipProzivoda != null || this.selektovaniTipProzivoda != undefined) {
        this.apiService.saveProizvod(this.debljina, this.duzina, this.sirina, this.visina, this.selektovaniTipProzivoda.id).then(
          response => {
            this.openSnackBar("Proizvod uspesno sacuvan.");
            this.dialogRef.close(true);
          }, errRes => {
            this.openSnackBar("Greska u slanju zahteva.");
          }
        )
      } else {
        this.openSnackBar("Tip proizvoda nije selektovan.");
      }
    }
  }

  onNazad() {
    this.dialogRef.close();
  }

  ucitajTipoveProizvoda() {
    this.apiService.getTipoviProizvoda().then(
      tipovi => {
        this.tipoviProizvoda = [];
        this.tipoviProizvoda = tipovi;
      }, errRes => {
        this.openSnackBar("Greska pri zahtevanju tipova prozivoda za combo box.");
      }
    )
  }

  validateFields() {
    if (this.debljina == "") {
      this.debljinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.debljina))) {
      this.debljinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.debljinaErrorMessage = "";

    if (this.duzina == "") {
      this.duzinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.duzina))) {
      this.duzinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.duzinaErrorMessage = "";

    if (this.sirina == "") {
      this.sirinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.sirina))) {
      this.sirinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.sirinaErrorMessage = "";

    if (this.visina == "") {
      this.visinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.visina))) {
      this.visinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.visinaErrorMessage = "";

    return true;
  }
}
