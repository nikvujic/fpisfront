import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ApiService } from '../api.service';
import { ProizvodEntry } from '../proizvod-entry.model';
import { Proizvod } from '../proizvod.model';
import { TipProizvoda } from '../tip-proizvoda.model';
import { NoviProizvodComponent } from './novi-proizvod/novi-proizvod.component';

@Component({
  selector: 'app-prost-slucaj',
  templateUrl: './prost-slucaj.component.html',
  styleUrls: ['./prost-slucaj.component.scss']
})
export class ProstSlucajComponent implements OnInit {

  proizvodId: string = "";
  proizvodIdErrorMessage: string = "";

  debljina: string = "";
  debljinaErrorMessage: string = "";

  duzina: string = "";
  duzinaErrorMessage: string = "";

  sirina: string = "";
  sirinaErrorMessage: string = "";

  visina: string = "";
  visinaErrorMessage: string = "";

  proizvodiTableData: ProizvodEntry[] = [];
  proizvodiDisplayColumns: string[] = ['ID', 'debljina', 'duzina', 'sirina', 'visina', 'tip'];

  tipoviProizvoda: TipProizvoda[]= [];

  selektovaniTipProzivoda: TipProizvoda;
  selektovaniTipProzivodaID: number;

  selektovaniProizvod: Proizvod;

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.ucitajTipoveProizvoda();
  }

  openSnackBar(message: string, action = "") {
    this.snackBar.open(message, action, {duration: 5000});
  }

  onNoviProizvod() {
    const dialogRef = this.dialog.open(NoviProizvodComponent, {
      height: '500px',
      width: '460px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result) {
          console.log(result);
        }
      }
    });
  }

  onPretraga() {
    this.proizvodiTableData = [];
    if (this.proizvodId == "") {
      this.apiService.getProizvodi().then(
        proizvodi => {
          const tempTableData = [];

          proizvodi.forEach(proizvod => {
            tempTableData.push(this.proizvodToEntry(proizvod));
          });

          this.proizvodiTableData = tempTableData;
        }, errRes => {
          this.openSnackBar("Proizvodi nisu pronadjeni. Proverite internet konekciju.");
        }
      )
    } else {
      if (this.validateProizvodId()) {
        this.apiService.getProizvod(+this.proizvodId).then(
          proizvod => {
            const tempTableData = [...this.proizvodiTableData];

            tempTableData.push(this.proizvodToEntry(proizvod));

            this.proizvodiTableData = tempTableData;
          }, errRes => {
            this.openSnackBar("Proizvod nije pronadjen. Proverite internet konekciju.");
          }
        )
      }
    }
  }

  validateProizvodId() {
    if (!Number.isInteger(Number(this.proizvodId))) {
      this.proizvodIdErrorMessage = "Polje nije validno.";
      return false;
    }

    if (Number(this.proizvodId) == 0) {
      this.proizvodIdErrorMessage = "Polje nije validno.";
      return false;
    }

    if (this.proizvodId.charAt(0) == '0') {
      this.proizvodIdErrorMessage = "Polje nije validno.";
      return false;
    }

    this.proizvodIdErrorMessage = "";
    return true;
  }

  onTableRowSelect(proizvodEntry: ProizvodEntry) {
    this.proizvodiTableData.forEach(proizvod => {
      proizvod.selected = false;
    });

    proizvodEntry.selected = true;

    this.selektovaniProizvod = this.entryToProizvod(proizvodEntry);
    this.debljina = this.selektovaniProizvod.debljina;
    this.duzina = this.selektovaniProizvod.duzina;
    this.sirina = this.selektovaniProizvod.sirina;
    this.visina = this.selektovaniProizvod.visina;
    this.selektovaniTipProzivoda = this.selektovaniProizvod.tipProizvoda;
    this.selektovaniTipProzivodaID = this.selektovaniTipProzivoda.id;
  }

  entryToProizvod(entry: ProizvodEntry) {
    let proizvod = new Proizvod(
      entry.id,
      entry.debljina,
      entry.duzina,
      entry.sirina,
      entry.visina,
      entry.tipProizvoda
    )

    return proizvod;
  }

  proizvodToEntry(proizvod: Proizvod) {
    let entry = new ProizvodEntry(
      proizvod.id,
      proizvod.debljina,
      proizvod.duzina,
      proizvod.sirina,
      proizvod.visina,
      proizvod.tipProizvoda,
      false
    )

    return entry;
  }

  validateFields() {
    if (this.debljina == "") {
      this.debljinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.debljina))) {
      this.debljinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.debljinaErrorMessage = "";

    if (this.duzina == "") {
      this.duzinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.duzina))) {
      this.duzinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.duzinaErrorMessage = "";

    if (this.sirina == "") {
      this.sirinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.sirina))) {
      this.sirinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.sirinaErrorMessage = "";

    if (this.visina == "") {
      this.visinaErrorMessage = "Polje nije popunjeno.";
      return false;
    }

    if (Number.isNaN(Number(this.visina))) {
      this.visinaErrorMessage = "Polje nije broj.";
      return false;
    }

    this.visinaErrorMessage = "";

    return true;
  }

  onTipProzivodaSelect(tipProzivoda) {
    this.selektovaniTipProzivoda = tipProzivoda;
  }

  onIzmeni() {
    if (this.selektovaniProizvod == undefined || this.selektovaniProizvod == null) {
      this.openSnackBar("Proizvod nije selektovan.");
    } else {
      if (this.validateFields()) {
        if (this.selektovaniTipProzivoda != null || this.selektovaniTipProzivoda != undefined) {
          this.apiService.editProizvod(this.selektovaniProizvod.id, this.debljina, this.duzina, this.sirina, this.visina, this.selektovaniTipProzivoda.id).then(
            response => {
              this.openSnackBar("Proizvod uspesno izmenjen.");

              const tempTableData = [];

              this.proizvodiTableData.forEach(proizvodEntry => {
                if (proizvodEntry.id != this.selektovaniProizvod.id) {
                  tempTableData.push(proizvodEntry);
                } else {
                  tempTableData.push(new ProizvodEntry(this.selektovaniProizvod.id, this.debljina, this.duzina, this.sirina, this.visina, this.selektovaniTipProzivoda, false));
                }
              });

              this.proizvodiTableData = tempTableData;

              this.debljina = "";
              this.debljinaErrorMessage = "";

              this.duzina = "";
              this.duzinaErrorMessage = "";

              this.sirina = "";
              this.sirinaErrorMessage = "";

              this.visina = "";
              this.visinaErrorMessage = "";

              this.selektovaniTipProzivoda = undefined;
              this.selektovaniTipProzivodaID = undefined;
              this.selektovaniProizvod = undefined;
            }, errRes => {
              this.openSnackBar("Greska u slanju zahteva.");
            }
          )
        } else {
          this.openSnackBar("Tip proizvoda nije selektovan.");
        }
      }
    }
  }

  onObrisi() {
    if (this.selektovaniProizvod == undefined || this.selektovaniProizvod == null) {
      this.openSnackBar("Proizvod nije selektovan.");
    } else {
      this.apiService.deleteProizvod(this.selektovaniProizvod.id).then(
        response => {
          this.openSnackBar("Proizvod uspesno obrisan.");

          const tempTableData = [];

          this.proizvodiTableData.forEach(proizvodEntry => {
            if (proizvodEntry.id != this.selektovaniProizvod.id) {
              tempTableData.push(proizvodEntry);
            }
          });

          this.proizvodiTableData = tempTableData;

          this.proizvodId = "";
          this.proizvodIdErrorMessage = "";

          this.debljina = "";
          this.debljinaErrorMessage = "";

          this.duzina = "";
          this.duzinaErrorMessage = "";

          this.sirina = "";
          this.sirinaErrorMessage = "";

          this.visina = "";
          this.visinaErrorMessage = "";

          this.selektovaniTipProzivoda = undefined;
          this.selektovaniTipProzivodaID = undefined;

          this.selektovaniProizvod = undefined;
        }, errRes => {
          this.openSnackBar("Greska u slanju zahteva.");
        }
      )
    }
  }

  ucitajTipoveProizvoda() {
    this.apiService.getTipoviProizvoda().then(
      tipovi => {
        this.tipoviProizvoda = [];
        this.tipoviProizvoda = tipovi;
      }, errRes => {
        this.openSnackBar("Greska pri zahtevanju tipova prozivoda za combo box.");
      }
    )
  }
}
