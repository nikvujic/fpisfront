import { Absence } from "./absence.model";
import { Attendance } from "./attendance.model";
import { Worker } from "./worker.model";

export class RadnaLista {
  constructor(
    public id: number,
    public datum: Date,
    public radniNalogId: number,
    public tovarniListId: number,
    public radnik: Worker,
    public odsustva: Absence[],
    public prisustva: Attendance[]
  ) {}
}

