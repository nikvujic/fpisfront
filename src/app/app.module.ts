import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProstSlucajComponent } from './prost-slucaj/prost-slucaj.component';
import { SlozenSlucajComponent } from './slozen-slucaj/slozen-slucaj.component';
import { WelcomePageCompoenentComponent } from './welcome-page-compoenent/welcome-page-compoenent.component';
import { MatButtonModule, MatDatepickerModule, MatDialogModule, MatDialogRef, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule, MatProgressSpinnerModule, MatSelectModule, MatSidenavModule, MatSnackBarModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { NoviProizvodComponent } from './prost-slucaj/novi-proizvod/novi-proizvod.component';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { NovaRadnaListaComponent } from './slozen-slucaj/nova-radna-lista/nova-radna-lista.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    ProstSlucajComponent,
    SlozenSlucajComponent,
    WelcomePageCompoenentComponent,
    NoviProizvodComponent,
    NovaRadnaListaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    HttpClientModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatProgressSpinnerModule,
  ],
  entryComponents: [NoviProizvodComponent, NovaRadnaListaComponent],
  providers: [DatePipe, {provide: MatDialogRef, useValue: {}}],
  bootstrap: [AppComponent]
})
export class AppModule { }
