export class Waybill {
  constructor(
    public id: number,
    public vrstaPosiljke: string,
    public nacinPrevoza: string,
    public radnikId: number,
    public klijentId: number
  ) {}
}

