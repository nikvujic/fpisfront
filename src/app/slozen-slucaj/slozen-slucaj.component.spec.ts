import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlozenSlucajComponent } from './slozen-slucaj.component';

describe('SlozenSlucajComponent', () => {
  let component: SlozenSlucajComponent;
  let fixture: ComponentFixture<SlozenSlucajComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlozenSlucajComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlozenSlucajComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
