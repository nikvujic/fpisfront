import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ApiService } from '../api.service';
import { NovaRadnaListaComponent } from './nova-radna-lista/nova-radna-lista.component';

@Component({
  selector: 'app-slozen-slucaj',
  templateUrl: './slozen-slucaj.component.html',
  styleUrls: ['./slozen-slucaj.component.scss']
})
export class SlozenSlucajComponent implements OnInit {

  @ViewChild(NovaRadnaListaComponent, {static: false}) radnaListaKompenenta: NovaRadnaListaComponent;

  radnaListaId: string = "";
  radnaListaIdErrorMessage: string = "";

  radnaListaIzabranId: string = "";

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action = "") {
    this.snackBar.open(message, action, {duration: 5000});
  }

  onNovaRadnaLista() {
    const dialogRef = this.dialog.open(NovaRadnaListaComponent, {
      height: '700px',
      width: '460px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        if (result) {

        }
      }
    });
  }

  onPronadjiRadnuListu() {
    this.radnaListaIzabranId = "";
    if (this.validateSifraRadneListe()) {
      this.apiService.getRadnaLista(+this.radnaListaId).then(
        radnaLista => {
          this.radnaListaIzabranId = "" + radnaLista.id;

          setTimeout(() => {
            this.radnaListaKompenenta.popuniPolja(radnaLista);
          }, 300);
        }, errRes => {
          this.openSnackBar("Radna lista nije pronadjena.");
        }
      );
    }
  }

  validateSifraRadneListe() {
    if (this.radnaListaId == "") {
      this.radnaListaIdErrorMessage = "Polje prazno.";
      return false;
    }

    if (!Number.isInteger(Number(this.radnaListaId))) {
      this.radnaListaIdErrorMessage = "Polje nije validno.";
      return false;
    }

    if (Number(this.radnaListaId) == 0) {
      this.radnaListaIdErrorMessage = "Polje nije validno.";
      return false;
    }

    if (this.radnaListaId.charAt(0) == '0') {
      this.radnaListaIdErrorMessage = "Polje nije validno.";
      return false;
    }

    this.radnaListaIdErrorMessage = "";
    return true;
  }

  onIzmeniRadnuListu() {
    if (this.radnaListaIzabranId != "") {
      let radnaLista = this.radnaListaKompenenta.napraviIVratiRadnuListu(+this.radnaListaIzabranId);

      this.apiService.editRadnaLista(radnaLista).then(
        () => {
          this.openSnackBar("Radna lista uspesno izmenjena.");
          this.radnaListaIzabranId = "";
        }, errRes => {
          this.openSnackBar("Izmena radne liste neuspesna.");
        });
      } else {
      this.openSnackBar("Prvo izaberite radnu listu.")
    }
  }

  onObrisiRadnuList() {
    if (this.radnaListaIzabranId != "") {
      this.apiService.deleteRadnaLista(+this.radnaListaIzabranId).then(
        () => {
          this.openSnackBar("Radna lista uspesno obrisana.");
          this.radnaListaIzabranId = "";
        }, errRes => {
          this.openSnackBar("Greska pri brisanju radne liste.");
        }
      );
    } else {
      this.openSnackBar("Prvo izaberite radnu listu.")
    }
  }
}
