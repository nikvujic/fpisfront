import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovaRadnaListaComponent } from './nova-radna-lista.component';

describe('NovaRadnaListaComponent', () => {
  let component: NovaRadnaListaComponent;
  let fixture: ComponentFixture<NovaRadnaListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovaRadnaListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovaRadnaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
