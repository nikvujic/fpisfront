import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { AbsenceEntry } from 'src/app/absence-entry.model';
import { Absence } from 'src/app/absence.model';
import { ApiService } from 'src/app/api.service';
import { AttendanceEntry } from 'src/app/attendance-entry.model';
import { Attendance } from 'src/app/attendance.model';
import { ProductionOrder } from 'src/app/production-order.model';
import { RadnaListaDto } from 'src/app/radna-lista-dto.model';
import { RadnaLista } from 'src/app/radna-lista.model';
import { Waybill } from 'src/app/waybill.model';
import { Worker } from 'src/app/worker.model';

@Component({
  selector: 'app-nova-radna-lista',
  templateUrl: './nova-radna-lista.component.html',
  styleUrls: ['./nova-radna-lista.component.scss']
})
export class NovaRadnaListaComponent implements OnInit {

  sifraRadnika: string = "";
  sifraRadnikaErrorMessage = "";

  sifraTovarnogLista: string = "";
  sifraTovarnogListaErrorMessage: string = "";

  sifraRadnogNaloga: string = "";
  sifraRadnogNalogaErrorMessage: string = "";

  opis: string = "";
  opisErrorMessage: string = "";

  datum: string = "";
  datumErrorMessage: string = "";

  datumPrisustva: string = "";
  datumPrisustvaErrorMessage: string = "";

  brojOdluke: string = "";
  brojOdlukeErrorMessage: string = "";

  datumOd: string = "";
  datumOdErrorMessage: string = "";

  datumDo: string = "";
  datumDoErrorMessage: string = "";

  radniciTableData: Worker[] = [];
  radniciDisplayColumns: string[] = ['ID', 'jmbg', 'imePrezime', 'radnoMesto'];

  tovarneListeTableData: Waybill[] = [];
  tovarneListeDisplayColumns: string[] = ['ID', 'vrstaPosiljke', 'nacinPrevoza', 'radnikId', 'klijentId'];

  radniNaloziTableData: ProductionOrder[] = [];
  radniNaloziDisplayColumns: string[] = ['ID', 'datum', 'brojPonude'];

  prisustvaTableData: AttendanceEntry[] = [];
  prisustvaDisplayColumns: string[] = ['RBR', 'vrstaPrisustva', 'datumPrisustva', 'opis'];

  odsustvaTableData: AbsenceEntry[] = [];
  odsustvaDisplayColumns: string[] = ['RBR', 'brojOdluke', 'vrstaOdsustva', 'datumOd', 'datumDo'];

  vrstePrisustva: string[] = ['pri. 1', 'pri. 2', 'pri. 3'];
  selektovanaVrstaPrisustva: string;

  vrsteOdsustva: string[] = ['ods. 1', 'ods. 2', 'ods. 3'];
  selektovanaVrstaOdsustva: string;

  selektovanoPrisustvo: AttendanceEntry;
  selektovanoOdsustvo: AbsenceEntry;

  @Input() openInComponent: boolean = false;

  constructor(
    private apiService: ApiService,
    private snackBar: MatSnackBar,
    private datepipe: DatePipe,
    private dialogRef: MatDialogRef<NovaRadnaListaComponent>,
  ) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action = "") {
    this.snackBar.open(message, action, {duration: 5000});
  }

  onPretragaRadnika() {
    if (this.validateRadnikId()) {
      this.apiService.getRadnik(+this.sifraRadnika).then(
        radnik => {
          this.radniciTableData = [];
          let newRadniciTabela: Worker[] = [];

          newRadniciTabela.push(radnik);

          this.radniciTableData = newRadniciTabela;
        }, errRes => {
          this.openSnackBar("Radnik nije pronadjen.");
        }
      )
    }
  }

  validateRadnikId() {
    this.sifraRadnikaErrorMessage = "";

    if (this.sifraRadnika == "") {
      this.sifraRadnikaErrorMessage = "Polje ne moze biti prazno.";
      return false;
    }

    if (!Number.isInteger(Number(this.sifraRadnika))) {
      this.sifraRadnikaErrorMessage = "Polje nije validno.";
      return false;
    }

    if (Number(this.sifraRadnika) == 0) {
      this.sifraRadnikaErrorMessage = "Polje nije validno.";
      return false;
    }

    if (this.sifraRadnika.charAt(0) == '0') {
      this.sifraRadnikaErrorMessage = "Polje nije validno.";
      return false;
    }

    this.sifraRadnikaErrorMessage = "";
    return true;
  }

  onPretragaTovarnogLista() {
    if (this.validateTovarnaListaId()) {
      this.apiService.getTovarnaLista(+this.sifraTovarnogLista).then(
        tovarnaLista => {
          this.tovarneListeTableData = [];
          let newTovarneListeTabela: Waybill[] = [];

          newTovarneListeTabela.push(tovarnaLista);

          this.tovarneListeTableData = newTovarneListeTabela;
        }, errRes => {
          this.openSnackBar("Tovarna lista nije pronadjena.")
        }
      )
    }
  }

  validateTovarnaListaId() {
    this.sifraTovarnogListaErrorMessage = "";

    if (this.sifraTovarnogLista == "") {
      this.sifraTovarnogListaErrorMessage = "Polje ne moze biti prazno.";
      return false;
    }

    if (!Number.isInteger(Number(this.sifraTovarnogLista))) {
      this.sifraTovarnogListaErrorMessage = "Polje nije validno.";
      return false;
    }

    if (Number(this.sifraTovarnogLista) == 0) {
      this.sifraTovarnogListaErrorMessage = "Polje nije validno.";
      return false;
    }

    if (this.sifraTovarnogLista.charAt(0) == '0') {
      this.sifraTovarnogListaErrorMessage = "Polje nije validno.";
      return false;
    }

    this.sifraTovarnogListaErrorMessage = "";
    return true;
  }

  onPretragaRadnogNaloga() {
    if (this.validateRadniNalogId()) {
      this.apiService.getRadniNalog(+this.sifraRadnogNaloga).then(
        radniNalog => {
          this.radniNaloziTableData = [];
          let newRadniNaloziTable: ProductionOrder[] = [];

          newRadniNaloziTable.push(radniNalog);

          this.radniNaloziTableData = newRadniNaloziTable;
        }, errRes => {
          this.openSnackBar("Radni nalog nije pronadjen.");
        }
      )
    }
  }

  validateRadniNalogId() {
    this.sifraRadnogNalogaErrorMessage = "";

    if (this.sifraRadnogNaloga == "") {
      this.sifraRadnogNalogaErrorMessage = "Polje ne moze biti prazno.";
      return false;
    }

    if (!Number.isInteger(Number(this.sifraRadnogNaloga))) {
      this.sifraRadnogNalogaErrorMessage = "Polje nije validno.";
      return false;
    }

    if (Number(this.sifraRadnogNaloga) == 0) {
      this.sifraRadnogNalogaErrorMessage = "Polje nije validno.";
      return false;
    }

    if (this.sifraRadnogNaloga.charAt(0) == '0') {
      this.sifraRadnogNalogaErrorMessage = "Polje nije validno.";
      return false;
    }

    this.sifraRadnogNalogaErrorMessage = "";
    return true;
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  validateDatum() {
    this.datumErrorMessage = "";

    if (this.datum == "") {
      this.datumErrorMessage = "Polje ne moze biti prazno.";
      return false;
    }

    if (!this.isValidDateFormat(this.datum)) {
      this.datumErrorMessage = "Pogresan format datuma.";
      return false;
    }

    if (Number(this.datum.split("-")[1]) == 0 || Number(this.datum.split("-")[1]) > 12) {
      this.datumErrorMessage = "Datum nije ispravan.";
      return false;
    }

    if (Number(this.datum.split("-")[2]) == 0 || Number(this.datum.split("-")[2]) > 31) {
      this.datumErrorMessage = "Datum nije ispravan.";
      return false;
    }

    this.datumErrorMessage = "";
    return true;
  }

  validateDatumPrisustva() {
    this.datumPrisustvaErrorMessage = "";

    if (this.datumPrisustva == "") {
      this.datumPrisustvaErrorMessage = "Polje prazno.";
      return false;
    }

    if (!this.isValidDateFormat(this.datumPrisustva)) {
      this.datumPrisustvaErrorMessage = "Pogresan format.";
      return false;
    }

    if (Number(this.datumPrisustva.split("-")[1]) == 0 || Number(this.datumPrisustva.split("-")[1]) > 12) {
      this.datumPrisustvaErrorMessage = "Pogresan format";
      return false;
    }

    if (Number(this.datumPrisustva.split("-")[2]) == 0 || Number(this.datumPrisustva.split("-")[2]) > 31) {
      this.datumPrisustvaErrorMessage = "Pogresan format.";
      return false;
    }

    this.datumPrisustvaErrorMessage = "";
    return true;
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }

  onDodajStavkuPrisustva() {
    if (this.validateDatumPrisustva()) {
      if ((this.selektovanaVrstaPrisustva != undefined || this.selektovanaVrstaPrisustva != null)) {
        const tempTableData = [...this.prisustvaTableData];

        this.prisustvaTableData = [];

        tempTableData.forEach(entry => {
          entry.selected = false;
        });

        let prisustvoEntry = new AttendanceEntry(
          undefined,
          0,
          new Date(this.datumPrisustva),
          this.opis,
          this.selektovanaVrstaPrisustva,
          false
        );

        this.datumPrisustva = "";
        this.opis = "";
        this.selektovanaVrstaPrisustva = undefined;
        this.selektovanoPrisustvo = undefined;

        tempTableData.push(prisustvoEntry);

        let counter = 1;
        tempTableData.forEach(entry => {
          entry.rbr = counter;
          counter++;
        });

        this.prisustvaTableData = tempTableData;
      } else {
        this.openSnackBar("Vrsta prisustva nije izabrana.");
      }
    }
  }

  onIzmeniStavkuPrisustva() {
    this.datumPrisustvaErrorMessage = "";

    if (!(this.selektovanoPrisustvo == null || this.selektovanoPrisustvo == undefined)) {
      if (this.validateDatumPrisustva()) {
        if ((this.selektovanaVrstaPrisustva != undefined || this.selektovanaVrstaPrisustva != null)) {
          const tempTableData = [...this.prisustvaTableData];

          this.prisustvaTableData = [];

          tempTableData.forEach(entry => {
            entry.selected = false;

            if (entry.rbr == this.selektovanoPrisustvo.rbr) {
              entry.datumPrisustva = new Date(this.datumPrisustva);
              entry.opis = this.opis;
              entry.vrstaPrisustva = this.selektovanaVrstaPrisustva;
            }
          });

          this.datumPrisustva = "";
          this.opis = "";
          this.selektovanaVrstaPrisustva = undefined;
          this.selektovanoPrisustvo = undefined;

          this.prisustvaTableData = tempTableData;
        } else {
          this.openSnackBar("Vrsta prisustva nije izabrana.");
        }
      }
    } else {
      this.openSnackBar("Stavka prisustva nije izabrana.");
    }
  }

  onObrisiStavkuPrisustva() {
    if (!(this.selektovanoPrisustvo == null || this.selektovanoPrisustvo == undefined)) {
      const tempTableData = [...this.prisustvaTableData];

      this.prisustvaTableData = [];

      tempTableData.forEach(entry => {
        entry.selected = false;
      });

      const index = tempTableData.indexOf(this.selektovanoPrisustvo, 0);
      if (index > -1) {
        tempTableData.splice(index, 1);
     }

      this.datumPrisustva = "";
      this.opis = "";
      this.selektovanaVrstaPrisustva = undefined;
      this.selektovanoPrisustvo = undefined;

      let counter = 1;
      tempTableData.forEach(entry => {
        entry.rbr = counter;
        counter++;
      });

      this.prisustvaTableData = tempTableData;
    } else {
      this.openSnackBar("Stavka prisustva nije izabrana.");
    }
  }

  convertPrisustvoToEntry(prisustvo: Attendance) {
    const prisustvoEntry = new AttendanceEntry(
      prisustvo.id,
      0,
      prisustvo.datumPrisustva,
      prisustvo.opis,
      prisustvo.vrstaPrisustva,
      false
    )

    return prisustvoEntry;
  }

  convertEntryToPrisustvo(prisustvoEntry: AttendanceEntry) {
    const prisustvo = new Attendance(
      prisustvoEntry.id,
      prisustvoEntry.datumPrisustva,
      prisustvoEntry.opis,
      prisustvoEntry.vrstaPrisustva
    )

    return prisustvo;
  }

  onPrisustvoTableRowSelect(prisustvoEntry: AttendanceEntry) {
    this.datumPrisustvaErrorMessage = "";

    this.prisustvaTableData.forEach(entry => {
      entry.selected = false;
    });

    prisustvoEntry.selected = true;

    this.selektovanoPrisustvo = prisustvoEntry;

    this.datumPrisustva = this.dateFormat(prisustvoEntry.datumPrisustva);
    this.selektovanaVrstaPrisustva = prisustvoEntry.vrstaPrisustva;
    this.opis = prisustvoEntry.opis;
  }


  onDodajStavkuOdsustva() {
    if (this.validateBrojOdluke() && this.validateDatumOd() && this.validateDatumDo()) {

      if (new Date(this.datumOd) > new Date(this.datumDo)) {
        this.openSnackBar("Datum od je nakon datuma do.")
        return;
      }

      if ((this.selektovanaVrstaOdsustva != undefined || this.selektovanaVrstaOdsustva != null)) {
        const tempTableData = [...this.odsustvaTableData];

        this.odsustvaTableData = [];

        tempTableData.forEach(entry => {
          entry.selected = false;
        });

        let odsustvoEntry = new AbsenceEntry(
          undefined,
          0,
          +this.brojOdluke,
          this.selektovanaVrstaOdsustva,
          new Date(this.datumOd),
          new Date(this.datumDo),
          false
        );

        this.brojOdluke = "";
        this.datumOd = "";
        this.datumDo = "";
        this.selektovanaVrstaOdsustva = undefined;
        this.selektovanoOdsustvo = undefined;

        tempTableData.push(odsustvoEntry);

        let counter = 1;
        tempTableData.forEach(entry => {
          entry.rbr = counter;
          counter++;
        });

        this.odsustvaTableData = tempTableData;
      } else {
        this.openSnackBar("Vrsta odsustva nije izabrana.");
      }
    }
  }

  onIzmeniStavkuOdsustva() {
    this.datumOdErrorMessage = "";
    this.datumDoErrorMessage = "";
    this.brojOdlukeErrorMessage = "";

    if (!(this.selektovanoOdsustvo == null || this.selektovanoOdsustvo == undefined)) {
      if (this.validateBrojOdluke() && this.validateDatumOd() && this.validateDatumDo()) {

        if (new Date(this.datumOd) > new Date(this.datumDo)) {
          this.openSnackBar("Datum od je nakon datuma do.")
          return;
        }

        if ((this.selektovanaVrstaOdsustva != undefined || this.selektovanaVrstaOdsustva != null)) {
          const tempTableData = [...this.odsustvaTableData];

          this.odsustvaTableData = [];

          tempTableData.forEach(entry => {
            entry.selected = false;

            if (entry.rbr == this.selektovanoOdsustvo.rbr) {
              entry.brojOdluke = +this.brojOdluke;
              entry.vrstaOdsustva = this.selektovanaVrstaOdsustva;
              entry.datumOd = new Date(this.datumOd);
              entry.datumDo = new Date(this.datumDo);
            }
          });

          this.brojOdluke = "";
          this.datumOd = "";
          this.datumDo = "";
          this.selektovanaVrstaOdsustva = undefined;
          this.selektovanoOdsustvo = undefined;

          this.odsustvaTableData = tempTableData;
        } else {
          this.openSnackBar("Vrsta odsustva nije izabrana.");
        }
      }
    } else {
      this.openSnackBar("Stavka odsustva nije izabrana.");
    }
  }

  onObrisiStavkuOdsustva() {
    if (!(this.selektovanoOdsustvo == null || this.selektovanoOdsustvo == undefined)) {
      const tempTableData = [...this.odsustvaTableData];

      this.odsustvaTableData = [];

      tempTableData.forEach(entry => {
        entry.selected = false;
      });

      const index = tempTableData.indexOf(this.selektovanoOdsustvo, 0);
      if (index > -1) {
        tempTableData.splice(index, 1);
      }

      this.brojOdluke = "";
      this.datumOd = "";
      this.datumDo = "";
      this.selektovanaVrstaOdsustva = undefined;
      this.selektovanoOdsustvo = undefined;

      let counter = 1;
      tempTableData.forEach(entry => {
        entry.rbr = counter;
        counter++;
      });

      this.odsustvaTableData = tempTableData;
    } else {
      this.openSnackBar("Stavka odsustva nije izabrana.");
    }
  }

  onOdsustvoTableRowSelect(odsustvoEntry: AbsenceEntry) {
    this.datumOdErrorMessage = "";
    this.datumDoErrorMessage = "";
    this.brojOdlukeErrorMessage = "";

    this.odsustvaTableData.forEach(entry => {
      entry.selected = false;
    });

    odsustvoEntry.selected = true;

    this.selektovanoOdsustvo = odsustvoEntry;

    this.brojOdluke = "" + odsustvoEntry.brojOdluke;
    this.datumOd = this.dateFormat(odsustvoEntry.datumOd);
    this.datumDo = this.dateFormat(odsustvoEntry.datumDo);;
    this.selektovanaVrstaOdsustva = odsustvoEntry.vrstaOdsustva;
  }

  convertAbsenceToEntry(absence: Absence) {
    const absenceEntry = new AbsenceEntry(
      absence.id,
      0,
      absence.brojOdluke,
      absence.vrstaOdsustva,
      absence.datumOd,
      absence.datumDo,
      false
    )

    return absenceEntry;
  }

  convertEntryToAbsence(absenceEntry: AbsenceEntry) {
    const absence = new Absence(
      absenceEntry.id,
      absenceEntry.brojOdluke,
      absenceEntry.vrstaOdsustva,
      absenceEntry.datumOd,
      absenceEntry.datumDo
    )

    return absence;
  }

  validateBrojOdluke() {
    this.brojOdlukeErrorMessage = "";

    if (this.brojOdluke == "") {
      this.brojOdlukeErrorMessage = "Polje ne moze biti prazno.";
      return false;
    }

    if (!Number.isInteger(Number(this.brojOdluke))) {
      this.brojOdlukeErrorMessage = "Polje nije validno.";
      return false;
    }

    if (Number(this.brojOdluke) == 0) {
      this.brojOdlukeErrorMessage = "Polje nije validno.";
      return false;
    }

    if (this.brojOdluke.charAt(0) == '0') {
      this.brojOdlukeErrorMessage = "Polje nije validno.";
      return false;
    }

    this.brojOdlukeErrorMessage = "";
    return true;
  }

  validateDatumOd() {
    this.datumOdErrorMessage = "";

    if (this.datumOd == "") {
      this.datumOdErrorMessage = "Polje prazno.";
      return false;
    }

    if (!this.isValidDateFormat(this.datumOd)) {
      this.datumOdErrorMessage = "Pogresan format.";
      return false;
    }

    if (Number(this.datumOd.split("-")[1]) == 0 || Number(this.datumOd.split("-")[1]) > 12) {
      this.datumOdErrorMessage = "Pogresan format";
      return false;
    }

    if (Number(this.datumOd.split("-")[2]) == 0 || Number(this.datumOd.split("-")[2]) > 31) {
      this.datumOdErrorMessage = "Pogresan format.";
      return false;
    }

    this.datumOdErrorMessage = "";
    return true;
  }

  validateDatumDo() {
    this.datumDoErrorMessage = "";

    if (this.datumDo == "") {
      this.datumDoErrorMessage = "Polje prazno.";
      return false;
    }

    if (!this.isValidDateFormat(this.datumDo)) {
      this.datumDoErrorMessage = "Pogresan format.";
      return false;
    }

    if (Number(this.datumDo.split("-")[1]) == 0 || Number(this.datumDo.split("-")[1]) > 12) {
      this.datumDoErrorMessage = "Pogresan format";
      return false;
    }

    if (Number(this.datumDo.split("-")[2]) == 0 || Number(this.datumDo.split("-")[2]) > 31) {
      this.datumDoErrorMessage = "Pogresan format.";
      return false;
    }

    this.datumDoErrorMessage = "";
    return true;
  }

  onSacuvajRadnuListu() {
    if (this.radniciTableData.length == 0) {
      this.openSnackBar("Niste izabrali radnika.");
      return;
    }

    if (this.tovarneListeTableData.length == 0) {
      this.openSnackBar("Niste izabrali tovarni list.");
      return;
    }

    if (this.radniNaloziTableData.length == 0) {
      this.openSnackBar("Niste izabrali radni nalog.")
    }

    if (!this.validateDatum()) {
      return;
    }

    const prisustva: Attendance[] = [];
    const odsustva: Absence[] = [];

    this.prisustvaTableData.forEach(entry => {
      prisustva.push(this.convertEntryToPrisustvo(entry));
    });

    this.odsustvaTableData.forEach(entry => {
      odsustva.push(this.convertEntryToAbsence(entry));
    });

    const radnaListaJson = new RadnaListaDto(
      this.datum,
      this.radniNaloziTableData[0].id,
      {"id": this.radniciTableData[0].id},
      this.tovarneListeTableData[0].id,
      odsustva,
      prisustva
    );

    console.log("Radna lista JSON", JSON.stringify(radnaListaJson));

    this.apiService.saveRadnaLista(radnaListaJson).then(
      resData => {
        this.openSnackBar(`Radna lista uspesno sacuvana. (${resData.id})`)
        this.dialogRef.close(true);
      }, errRes => {
        this.openSnackBar("Greska pri cuvanju radne liste.");
      }
    )
  }

  onNazad() {
    this.dialogRef.close();
  }

  async popuniPolja(radnaLista: RadnaLista) {
    const tovarniList = await this.apiService.getTovarnaLista(radnaLista.tovarniListId);

    this.tovarneListeTableData = [tovarniList];

    const radniNalog = await this.apiService.getRadniNalog(radnaLista.radniNalogId);

    this.radniNaloziTableData = [radniNalog];

    this.datum = this.dateFormat(radnaLista.datum);

    let prisustva: AttendanceEntry[] = [];

    radnaLista.prisustva.forEach(prisustvo => {
      prisustva.push(this.convertPrisustvoToEntry(prisustvo));
    });

    let pCounter = 1;
    prisustva.forEach(entry => {
      entry.rbr = pCounter;
      pCounter++;
    });

    this.prisustvaTableData = prisustva;

    let odsustva: AbsenceEntry[] = [];

    radnaLista.odsustva.forEach(odsustvo => {
      odsustva.push(this.convertAbsenceToEntry(odsustvo));
    });

    let oCounter = 1;
    odsustva.forEach(entry => {
      entry.rbr = oCounter;
      oCounter++;
    });

    this.odsustvaTableData = odsustva;

    this.radniciTableData = [radnaLista.radnik];
  }

  napraviIVratiRadnuListu(id: number) {
    if (this.radniciTableData.length == 0) {
      this.openSnackBar("Niste izabrali radnika.");
      return;
    }

    if (this.tovarneListeTableData.length == 0) {
      this.openSnackBar("Niste izabrali tovarni list.");
      return;
    }

    if (this.radniNaloziTableData.length == 0) {
      this.openSnackBar("Niste izabrali radni nalog.")
    }

    if (!this.validateDatum()) {
      return;
    }

    const prisustva: Attendance[] = [];
    const odsustva: Absence[] = [];

    this.prisustvaTableData.forEach(entry => {
      prisustva.push(this.convertEntryToPrisustvo(entry));
    });

    this.odsustvaTableData.forEach(entry => {
      odsustva.push(this.convertEntryToAbsence(entry));
    });

    const radnaLista = new RadnaLista(
      id,
      new Date(this.datum),
      this.radniNaloziTableData[0].id,
      this.tovarneListeTableData[0].id,
      this.radniciTableData[0],
      odsustva,
      prisustva
    );

    return radnaLista;
  }
}
