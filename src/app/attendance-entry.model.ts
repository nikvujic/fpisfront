export class AttendanceEntry {
  constructor(
    public id: number,
    public rbr: number,
    public datumPrisustva: Date,
    public opis: string,
    public vrstaPrisustva: string,
    public selected: boolean
  ) {}
}
