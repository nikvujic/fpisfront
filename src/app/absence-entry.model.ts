export class AbsenceEntry {
  constructor(
    public id: number,
    public rbr: number,
    public brojOdluke: number,
    public vrstaOdsustva: string,
    public datumOd: Date,
    public datumDo: Date,
    public selected: boolean
  ) {}
}
