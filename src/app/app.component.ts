import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  firstTabSelected = false;
  secondTabSelected = false;

  title = 'fpisFront';

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    // if (this.router.url == '/') {
    //   this.router.navigate(['']);
    // }
  }

  onProstSlucaj() {
    this.firstTabSelected = true;
    this.secondTabSelected = false;

    this.router.navigate(['prostSlucaj']);
  }

  onSlozenSlucaj() {
    this.firstTabSelected = false;
    this.secondTabSelected = true;

    this.router.navigate(['slozenSlucaj']);
  }
}
