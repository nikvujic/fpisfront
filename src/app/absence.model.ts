export class Absence {
  constructor(
    public id: number,
    public brojOdluke: number,
    public vrstaOdsustva: string,
    public datumOd: Date,
    public datumDo: Date
  ) {}
}
