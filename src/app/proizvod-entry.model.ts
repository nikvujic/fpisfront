import { TipProizvoda } from "./tip-proizvoda.model";

export class ProizvodEntry {
  constructor(
    public id: number,
    public debljina: string,
    public duzina: string,
    public sirina: string,
    public visina: string,
    public tipProizvoda: TipProizvoda,
    public selected: boolean
  ) {}
}

