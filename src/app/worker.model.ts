export class Worker {
  constructor(
    public id: number,
    public jmbg: string,
    public imePrezime: string,
    public radnoMesto: string
  ) {}
}

