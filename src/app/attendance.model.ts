export class Attendance {
  constructor(
    public id: number,
    public datumPrisustva: Date,
    public opis: string,
    public vrstaPrisustva: string
  ) {}
}
